var api = "https://www.dudooeat.com/maindudoo/api/Dudooapi/";

$.ajax({
    url: api+'getCitylist',
    data: {
        lang: 'zh'
    },
    type: 'POST',
    dataType : 'json',
})
    .done(function( getJson ) {
        for (var i = 0 ;i < getJson.cityList.length ; i++){
            document.getElementById("city_id").innerHTML+='<option value="'+getJson.cityList[i].id+'">'+getJson.cityList[i].name+'</option>';
        }
    })
    .fail(function( xhr, status, errorThrown ) {
        // console.log( 'Error: ' + errorThrown )
        // console.log( 'Status: ' + status )
        // console.dir( xhr )
    })

var request;
$("#form").submit(function(event){
    event.preventDefault();
    if (request) {
        request.abort();
    }
    var $form = $(this);
    var $inputs = $form.find("input, select, button, textarea");
    var check = ''
    if ($('#i1').val()) {
        check += '(POS)'
    }
    if ($('#i2').val()) {
        check += '(APP)'
    }
    if ($('#i3').val()) {
        check += '(部落格行銷)'
    }
    if ($('#i4').val()) {
        check += '(開店資訊顧問)'
    }
    request = $.ajax({
        url: api+"ContactRequest",
        type: "post",
        data: {
            story_name : $('#story_name').val(),
            no : $('#no').val(),
            name : $('#name').val(),
            position : $('#position').val(),
            phone : $('#phone').val(),
            email : $('#email').val(),
            city_id : $('#city_id').val(),
            address : $('#address').val(),
            body : check + $('#body').val()
            //passmail : true
        }
    });


    request.done(function (response, textStatus, jqXHR){
        //console.log("success");
        alert("您的表單已經成功發送，我們會盡快與您聯絡！");
        // window.location.reload();
        $.get("https://script.google.com/macros/s/AKfycbx1DeB33h-EuJKXnaPV5HnduxF0dGxHAWOB-yrQx88cJ_GRl0M/exec", {
            story_name : $('#story_name').val(),
            VAT_number : $('#no').val(),
            name : $('#name').val(),
            position : $('#position').val(),
            phone : $('#phone').val(),
            email : $('#email').val(),
            city_id : $('#city_id').val(),
            city_name : $('#city_id  option:selected').text(),
            address: $('#city_id  option:selected').text() + $('#address').val(),
            needs: check + $('#body').val(),
        },function(){
            window.location.reload();
        });
    });
    request.fail(function (jqXHR, textStatus, errorThrown){
        // console.log(textStatus, errorThrown);
        alert("抱歉，您表單似乎尚未完成！");
    });
    request.always(function () {
        $inputs.prop("disabled", false);
    });

});

function ValidateNumber(e, pnumber)
{
    if (!/^\d+$/.test(pnumber))
    {
        $(e).val(/^\d+/.exec($(e).val()));
    }
    return false;
}