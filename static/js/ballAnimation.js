$.fn.BallMoveRight = function(){
  this.each(function(){
    var _self = $(this);
    var _x = $(window).width() * Math.random()*0.3+0;
    var _y = $(window).height() * Math.random();
    var control= 0;
    var _alpha = 1.0 *1;
    _self.css({ "position":"absolute","top":_y + "px","left":_x+ control + "px"});

    delayGo();
    function delayGo(){
      var _delayTime = 1000;
      setTimeout(function(){ballGoMove();},_delayTime);
    }
    function ballGoMove(){
      var _goTime = 3000 * Math.random() + 4000;
      _self.animate({"opacity":_alpha},{duration: 2000})
         .delay(_goTime - 2000).animate({"opacity":0},{duration: 2000})
         .animate({"top":_y + 100 - Math.random()*200 + "px","left":_x + control - Math.random()*200 +  "px"/*,"margin-left":"80%"*/},{duration:_goTime, queue: false } );
      setTimeout(function(){delayGo();},_goTime);
    }
  })
  return this;
}
$.fn.BallMoveLeft = function(){
  this.each(function(){
    var _self = $(this);
    var _x = $(window).width() * Math.random()*0.3+0;
    var _y = $(window).height() * Math.random();
    var control= 0;
    var _alpha = 1.0 *1;
    _self.css({ "position":"absolute","top":_y + "px","right":_x + "px"});

    delayGo();
    function delayGo(){
      var _delayTime = 1000;
      setTimeout(function(){ballGoMove();},_delayTime);
    }
    function ballGoMove(){
      var _goTime = 3000 * Math.random() + 4000;
      _self.animate({"opacity":_alpha},{duration: 2000})
         .delay(_goTime - 2000).animate({"opacity":0},{duration: 2000})
         .animate({"top":_y + 100 - Math.random()*200 + "px","right":_x - Math.random()*200 +  "px"/*,"margin-left":"80%"*/},{duration:_goTime, queue: false } );
      setTimeout(function(){delayGo();},_goTime);
    }
  })
  return this;
}
  //right-balls